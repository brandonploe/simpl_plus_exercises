using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_SWITCHER_EXERCISES
{
    public class UserModuleClass_SWITCHER_EXERCISES : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SOURCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> CLEAR;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> GSTEMP;
        Crestron.Logos.SplusObjects.AnalogOutput SWITCHER;
        Crestron.Logos.SplusObjects.StringOutput SOURCE_TEXT;
        InOutArray<UShortParameter> DI;
        object SOURCE_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort NSOURCE = 0;
                
                
                __context__.SourceCodeLine = 167;
                NSOURCE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 168;
                
                    {
                    int __SPLS_TMPVAR__SWTCH_1__ = ((int)NSOURCE);
                    
                        { 
                        if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 172;
                            Trace( "1st Source") ; 
                            __context__.SourceCodeLine = 173;
                            SWITCHER  .Value = (ushort) ( DI[ 1 ] .Value ) ; 
                            __context__.SourceCodeLine = 174;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 1 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 178;
                            Trace( "2nd Source") ; 
                            __context__.SourceCodeLine = 179;
                            SWITCHER  .Value = (ushort) ( DI[ 2 ] .Value ) ; 
                            __context__.SourceCodeLine = 180;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 2 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 184;
                            Trace( "3rd Source") ; 
                            __context__.SourceCodeLine = 185;
                            SWITCHER  .Value = (ushort) ( DI[ 3 ] .Value ) ; 
                            __context__.SourceCodeLine = 186;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 3 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 190;
                            Trace( "4th Source") ; 
                            __context__.SourceCodeLine = 191;
                            SWITCHER  .Value = (ushort) ( DI[ 4 ] .Value ) ; 
                            __context__.SourceCodeLine = 192;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 4 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 196;
                            Trace( "5th Source") ; 
                            __context__.SourceCodeLine = 197;
                            SWITCHER  .Value = (ushort) ( DI[ 5 ] .Value ) ; 
                            __context__.SourceCodeLine = 198;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 5 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 202;
                            Trace( "6th Source") ; 
                            __context__.SourceCodeLine = 203;
                            SWITCHER  .Value = (ushort) ( DI[ 6 ] .Value ) ; 
                            __context__.SourceCodeLine = 204;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 6 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 208;
                            Trace( "7th Source") ; 
                            __context__.SourceCodeLine = 209;
                            SWITCHER  .Value = (ushort) ( DI[ 7 ] .Value ) ; 
                            __context__.SourceCodeLine = 210;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 7 ]  ) ; 
                            } 
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) ) ) ) 
                            { 
                            __context__.SourceCodeLine = 214;
                            Trace( "8th Source") ; 
                            __context__.SourceCodeLine = 215;
                            SWITCHER  .Value = (ushort) ( DI[ 8 ] .Value ) ; 
                            __context__.SourceCodeLine = 216;
                            SOURCE_TEXT  .UpdateValue ( GSTEMP [ 8 ]  ) ; 
                            } 
                        
                        } 
                        
                    }
                    
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object CLEAR_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 224;
            SWITCHER  .Value = (ushort) ( DI[ 9 ] .Value ) ; 
            __context__.SourceCodeLine = 225;
            SOURCE_TEXT  .UpdateValue ( "Off"  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    
    SOURCE = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SOURCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SOURCE__DigitalInput__ + i, SOURCE__DigitalInput__, this );
        m_DigitalInputList.Add( SOURCE__DigitalInput__ + i, SOURCE[i+1] );
    }
    
    CLEAR = new InOutArray<DigitalInput>( 1, this );
    for( uint i = 0; i < 1; i++ )
    {
        CLEAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( CLEAR__DigitalInput__ + i, CLEAR__DigitalInput__, this );
        m_DigitalInputList.Add( CLEAR__DigitalInput__ + i, CLEAR[i+1] );
    }
    
    SWITCHER = new Crestron.Logos.SplusObjects.AnalogOutput( SWITCHER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( SWITCHER__AnalogSerialOutput__, SWITCHER );
    
    GSTEMP = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        GSTEMP[i+1] = new Crestron.Logos.SplusObjects.StringInput( GSTEMP__AnalogSerialInput__ + i, GSTEMP__AnalogSerialInput__, 25, this );
        m_StringInputList.Add( GSTEMP__AnalogSerialInput__ + i, GSTEMP[i+1] );
    }
    
    SOURCE_TEXT = new Crestron.Logos.SplusObjects.StringOutput( SOURCE_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOURCE_TEXT__AnalogSerialOutput__, SOURCE_TEXT );
    
    DI = new InOutArray<UShortParameter>( 9, this );
    for( uint i = 0; i < 9; i++ )
    {
        DI[i+1] = new UShortParameter( DI__Parameter__ + i, DI__Parameter__, this );
        m_ParameterList.Add( DI__Parameter__ + i, DI[i+1] );
    }
    
    
    for( uint i = 0; i < 8; i++ )
        SOURCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( SOURCE_OnPush_0, false ) );
        
    for( uint i = 0; i < 1; i++ )
        CLEAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( CLEAR_OnPush_1, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_SWITCHER_EXERCISES ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint SOURCE__DigitalInput__ = 0;
const uint CLEAR__DigitalInput__ = 8;
const uint GSTEMP__AnalogSerialInput__ = 0;
const uint SWITCHER__AnalogSerialOutput__ = 0;
const uint SOURCE_TEXT__AnalogSerialOutput__ = 1;
const uint DI__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
